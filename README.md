## GitLab Helm Charts

This repository collects GitLab's official Helm charts from their individual
repos and automatically publish them to our Helm repo, located at
[charts.gitlab.io](https://charts.gitlab.io). [Helm](https://helm.sh/) is a
package manager for Kubernetes, making it easier to deploy, upgrade, and
maintain software like GitLab.

The charts collected and published currently by this repository are the following:

| Chart name   | Status       | Remark
|--------------|--------------|----------|
| [gitlab](https://gitlab.com/charts/gitlab.git)       | GA | Suitable for small deployments. |
| [gitlab-runner](https://gitlab.com/charts/gitlab-runner.git) | GA | Deploys the GitLab CI/CD Runner. |
| [gitlab-omnibus](https://gitlab.com/charts/gitlab-omnibus.git) | Deprecated | Replaced by the [GitLab chart](#gitlab-chart). |
| [kubernetes-gitlab-demo](https://gitlab.com/charts/kubernetes-gitlab-demo.git) | Deprecated | Should not be used. |
| [auto-deploy-app](https://gitlab.com/gitlab-org/charts/auto-deploy-app) | GA | Used by [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/topics/autodevops/index.md). |
| [plantuml](https://gitlab.com/gitlab-org/charts/plantuml) | GA | Used for GitLab PlantUML integration |

More information is available in our [chart documentation](https://docs.gitlab.com/charts/).

### Usage

To use the charts, the Helm tool must be installed and initialized. The best
place to start is by reviewing the [Helm Quick Start Guide](https://github.com/kubernetes/helm/blob/master/docs/quickstart.md).

Installation instructions, including configuration options, can be found in our [documentation](http://docs.gitlab.com/ce/install/kubernetes/).

### GitLab Helm Charts Issue Tracker

Issues related to the Charts can be logged for the respective chart at: <https://gitlab.com/groups/charts/-/issues>

### Contributing to the Charts

We welcome contributions and improvements. The source repo for our Helm Charts can be found here: <https://gitlab.com/charts/charts.gitlab.io>

Please see the [contribution guidelines](CONTRIBUTING.md)
